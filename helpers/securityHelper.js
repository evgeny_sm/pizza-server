'user strict';

const crypto = require('crypto');
const securityHelper = {
    makeSaltHash: function (pwd, salt) {
        const hash = salt + crypto.createHash('md5').update(salt + pwd).digest('hex');
        return hash;
    },
    createHash: function (pwd) {
        const buf = crypto.randomBytes(8);
        const salt = buf.toString('hex');
        return this.makeSaltHash(pwd, salt);
    },
    checkPassword: function (pwd, hash) {
        const salt = hash.substr(0, 16);
        return this.makeSaltHash(pwd, salt) === hash;
    }
};

module.exports = securityHelper;