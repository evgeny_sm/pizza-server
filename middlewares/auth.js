'use strict';
let auth = {
    isAuth: function (req, res, next){
        req.isAuthenticated()
            ? next()
            : res.sendStatus(401); //Unauthorized
    },
    hasAccess: function (requiredAccess) {
        return function (req, res, next) {
            if(!req.isAuthenticated()) {
                return res.sendStatus(401); //Unauthorized
            }
            for(const group of req.user.userGroups) {
                for(const oAcces of group.userGroupAccesses) {
                    if(oAcces.code === requiredAccess) {
                        return next();
                    }
                }
            }
            return res.sendStatus(403); //Forbidden
        }
    }
};
module.exports = auth;