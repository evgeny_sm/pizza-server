'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('catalogs', 'picture_detail', {
            type: Sequelize.INTEGER
        })
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('catalogs', 'picture_detail');
    }
};
