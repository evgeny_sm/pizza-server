# PizzaServer

### Запуск сервера
1. В PostgreSQL создать базу и указать параметры подключения в файле /config/config.json
2. Установить зависимости npm install
3. Применить миграции(требуется sequelize-cli) sequelize db:migrate
4. Загрузить тестовые данные sequelize db:seed:all
5. Запустить сервер npm start