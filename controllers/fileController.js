'use strict';
let models = require('../models/index');

let controller = {
    getById: function (req, res, next) {
        let id = +req.params.id;
        models.file.findOne({where: {id: id}}).then(result => {
            if(!result) return res.sendStatus(404);
            res.send(result);
        });
    },

    addFile: function (req, res, next) {
        let oFile = {
            name: req.file.originalname,
            mimetype: req.file.mimetype,
            size: req.file.size,
            path: req.file.path,
        };
        models.file.create(oFile).then(result => {
            res.send(result);
        });
    },

    deleteFile: function (req, res, next) {
        let id = +req.params.id;
        models.file.findOne({where: {id: id}}).then(result => {
            if(!result) return res.sendStatus(404);

            result.destroy({force: true}).then(result => {
                res.send(result);
            });
        });
    },

    getList: function (req, res, next) {
        let order = [
            ['id', 'asc']
        ];
        models.file.findAll({
            order: order
        }).then(result => {
            res.send(result);
        });
    }
};
module.exports = controller;