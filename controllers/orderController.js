'use strict';
let models = require('../models/index');
let basketController = require("./basketController.js");

let controller = {
    getList: function (req, res, next) {
        let order = [
            ['id', 'asc']
        ];
        let limit = null;
        let offset = null;
        if(req.query.limit) limit = +req.query.limit;
        if(req.query.offset) offset = +req.query.offset;
        models.order.findAll({
            include: [{
                model: models.orderItem
            }]
        }).then(result => {
            res.send(result || {count: 0, rows: []});
        });
    },

    getById: function (req, res, next) {
        let orderId = +req.params.id;
        models.order.findOne({
            where:{id: orderId},
            include: [{
                model: models.orderItem
            }]
        }).then(order => {
            if(!order) return res.sendStatus(404);
            res.send(order);
        });
    },

    getByIdAndToken: function (req, res, next) {
        let orderId = +req.params.id;
        let token = req.params.token;
        models.order.findOne({
            where:{id: orderId, token: token},
            include: [{
                model: models.orderItem
            }]
        }).then(order => {
            if(!order) return res.sendStatus(404);
            res.send(order);
        });
    },

    add: function (req, res, next) {
        let orderId = null;
        let items = [];
        let aID = Object.keys(req.body.items);
        models.catalog.findAll({
            where: {id: aID}
        }).then(result => {
            let sum = 0;
            items = result;
            for(let item of items) {
                sum += item.price * req.body.items[item.id];
            }
            let order = {
                name: req.body.form.name,
                street: req.body.form.address.street,
                house: req.body.form.address.house,
                apart: req.body.form.address.apart,
                phone: req.body.form.phone,
                email: req.body.form.email,
                note: req.body.form.note,
                sum: sum
            };
            return models.order.create(order);
        }).then(order => {
            orderId = order.id;
            let list = [];
            for(let item of items) {
                list.push({
                    orderId: order.id,
                    catalogItemId: item.id,
                    name: item.name,
                    price: item.price,
                    count: req.body.items[item.id],
                    sum: item.price * req.body.items[item.id],
                });
            }
            return models.orderItem.bulkCreate(list);
        })
        .then(result => {
            return models.order.findOne({
                where:{id: orderId},
                include: [{
                    model: models.orderItem
                }]
            });
        })
        .then(order => {
            res.send(order);
        })
        .catch(error => {
            console.log(error);
            res.sendStatus(500);
        });
    },

    save: function (req, res, next) {

    },

    delete: function (req, res, next) {

    }
};
module.exports = controller;