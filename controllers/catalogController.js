'use strict';
let models = require('../models/index');

let controller = {
    getList: function (req, res, next) {
        let order = [
            ['id', 'asc']
        ];
        let limit = null;
        let offset = null;
        if(req.query.limit) limit = +req.query.limit;
        if(req.query.offset) offset = +req.query.offset;
        models.catalog.findAndCountAll({
            order: order,
            include: [{
                model: models.file,
                as: 'oPicture',
            }, {
                model: models.file,
                as: 'oPictureDetail',
            }],
            limit: limit,
            offset: offset,
        }).then(result => {
            res.send(result || {count: 0, rows: []});
        });
    },

    getById: function (req, res, next) {
        let id = +req.params.id;
        models.catalog.findOne({
            where: {id: id},
            include: [{
                model: models.file,
                as: 'oPicture',
            }, {
                model: models.file,
                as: 'oPictureDetail',
            }]
        }).then(result => {
            if(!result) return res.sendStatus(404);
            res.send(result);
        });
    },

    addItem: function (req, res, next) {
        let item = {
            name: req.body.item.name,
            price: req.body.item.price,
            description: req.body.item.description,
            picture: req.body.item.picture,
            picture_detail: req.body.item.picture_detail,
        };
        models.catalog.create(item)
        .then(result => {
            return models.catalog.findOne({
                where: {id: result.id},
                include: [{
                    model: models.file,
                    as: 'oPicture',
                }, {
                    model: models.file,
                    as: 'oPictureDetail',
                }]
            });
        })
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            res.sendStatus(500);
        });
    },

    saveItem: function (req, res, next) {
        let item = {
            name: req.body.item.name,
            price: req.body.item.price,
            description: req.body.item.description,
            picture: req.body.item.picture,
            picture_detail: req.body.item.picture_detail,
        };
        let id = +req.params.id;
        models.catalog.findOne({where: {id: id}}).then(result => {
            if(!result) throw 'Not found';
            return result.update(item);
        })
        .then(result => {
            return models.catalog.findOne({
                where: {id: result.id},
                include: [{
                    model: models.file,
                    as: 'oPicture',
                }, {
                    model: models.file,
                    as: 'oPictureDetail',
                }]
            });
        })
        .then(result => {
            res.send(result);
        }).catch(error => {
            res.sendStatus(500);
        });
    },

    deleteItem: function (req, res, next) {
        let id = +req.params.id;
        models.catalog.findOne({where: {id: id}}).then(result => {
            if(!result) throw 'Not found';
            result.destroy({force: true});
        }).then(result => {
            res.send(result);
        }).catch(error => {
            res.sendStatus(500);
        });
    }
};
module.exports = controller;