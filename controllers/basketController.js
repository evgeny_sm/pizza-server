'use strict';
let models = require('../models/index');

let basketController = {
    deleteItem: function (req, res, next) {
        const aBasket = req.basket;
        const itemId = +req.params.id;
        let index = aBasket.findIndex(function (element) {
            return element.id === itemId;
        });
        if (index !== -1) {
            aBasket.splice(index, 1);
        }
        res.sendStatus(200);
    },

    deleteAll: function (req, res, next) {
        const aBasket = req.basket;
        aBasket.splice(0);
        res.sendStatus(200);
    },

    saveItem: function (req, res, next) {
        const aBasket = req.basket;
        const itemId = +req.params.id;
        const itemCount = +req.body.count > 0 ? +req.body.count : 1;

        let index = aBasket.findIndex(function (element) {
            return element.id === itemId;
        });
        if (index !== -1) {
            aBasket[index].count = itemCount;
        }
        res.sendStatus(200);
    },

    addItem: function (req, res, next) {
        const aBasket = req.basket;
        const item = {
            id: +req.body.id,
            count: +req.body.count > 0 ? +req.body.count : 1
        };
        models.catalog.findOne({where: {id: item.id}}).then(
            result => {
                let index = aBasket.findIndex(function (element) {
                    return element.id === item.id;
                });
                if (index === -1) {
                    aBasket.push(item);
                }
                res.sendStatus(200);
            },
            error => {
                res.sendStatus(500);
            }
        ).catch(error => {
            res.sendStatus(500);
        });
    },

    getBasket: function (req, res, next) {
        // initBasket(req);
        const aBasket = req.session.basket;
        let aID = [];
        for (let item of aBasket) {
            aID.push(item.id);
        }
        models.catalog.findAll({
            where: {id: aID},
            include: [{
                model: models.file,
                as: 'oPicture',
            }, {
                model: models.file,
                as: 'oPictureDetail',
            }]
        }).then(
            result => {
                let aData = [];
                for (let item of result) {
                    let index = aBasket.findIndex(function (element) {
                        return element.id === item.id;
                    });
                    if (index === -1) continue;

                    aData.push({item: item, count: aBasket[index].count})
                }
                res.send(aData);
            },
            error => {
                res.sendStatus(500);
            }
        ).catch(error => {
            res.sendStatus(500);
        });
    }
};
module.exports = basketController;