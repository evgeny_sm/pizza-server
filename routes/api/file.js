'use strict';
let express = require('express');
let router = express.Router();
let models = require('../../models/index');
let auth = require('../../middlewares/auth');
let fileController = require('../../controllers/fileController');

let multer = require('multer');
let upload = multer({dest: 'upload'});
/*
 * result*/
router.get('/:id', fileController.getById);

router.post('/',
    auth.hasAccess('file.upload'),
    upload.single('uploadFile'),
    fileController.addFile
);

router.delete('/:id', auth.hasAccess('file.upload'), fileController.deleteFile);

router.get('/', fileController.getList);

module.exports = router;
