'use strict';
let express = require('express');
let router = express.Router();
let models = require('../../models/index');
let auth = require('../../middlewares/auth');
let catalogController = require('../../controllers/catalogController');

router.get('/:id', catalogController.getById);

router.post('/', auth.hasAccess('catalog.edit'), catalogController.addItem);
router.put('/:id', auth.hasAccess('catalog.edit'), catalogController.saveItem);
router.delete('/:id', auth.hasAccess('catalog.edit'), catalogController.deleteItem);
router.get('/', catalogController.getList);

module.exports = router;
