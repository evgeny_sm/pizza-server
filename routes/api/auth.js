let express = require('express');
let router = express.Router();

let auth      = require('../../middlewares/auth');
router.use('/info', auth.isAuth, function (req, res, next) {
    res.send(req.user);
});

router.get('/', function (req, res, next) {
    res.send('api auth...');
});

let passport = require('passport');

router.post('/login', passport.authenticate('local'), function (req, res, next) {
    res.send(req.user);
    //res.send({message: 'success'});
});

router.get('/logout', function(req, res){
    req.logout();
    res.send({message: 'success'});
});

router.get('/testauth', auth.isAuth, function (req, res, next) {
    res.send('is auth');
});

router.get('/testaccess1', auth.hasAccess('catalog.edit'), function (req, res, next) {
    res.send('has catalog.edit');
});
router.get('/testaccess2', auth.hasAccess('cataog.edixt'), function (req, res, next) {
    res.send('has catalog.edit');
});

module.exports = router;
