'use strict';
let express = require('express');
let router = express.Router();
let basketController = require('../../controllers/basketController');

router.use(function(req, res, next) {
    if(typeof req.session.basket === 'undefined') {
        req.session.basket = [];
    }
    req.basket = req.session.basket;
    next();
});

router.get('/', basketController.getBasket);
router.post('/', basketController.addItem);
router.put('/:id', basketController.saveItem);
router.delete('/:id', basketController.deleteItem);
router.delete('/', basketController.deleteAll);



module.exports = router;
