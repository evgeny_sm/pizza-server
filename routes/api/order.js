'use strict';
let express = require('express');
let router = express.Router();
let models = require('../../models/index');
let auth = require('../../middlewares/auth');
let orderController = require('../../controllers/orderController');

router.get('/:id/:token', orderController.getByIdAndToken);
router.get('/:id', auth.hasAccess('order.view'), orderController.getById);

router.post('/', orderController.add);
router.put('/:id', auth.hasAccess('order.edit'), orderController.save);
router.delete('/:id', auth.hasAccess('order.edit'), orderController.delete);
router.get('/', auth.hasAccess('order.view'), orderController.getList);

module.exports = router;
