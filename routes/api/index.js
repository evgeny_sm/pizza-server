'use strict';
let express = require('express');
let router = express.Router();

let auth = require('./auth');
let catalog = require('./catalog');
let file = require('./file');
let miniBasket = require('./mini-basket');
let order = require('./order');

router.use('/auth', auth);
router.use('/catalog', catalog);
router.use('/file', file);
router.use('/mini-basket', miniBasket);
router.use('/order', order);

router.get('/', function(req, res, next) {
    res.send('api...');
});

module.exports = router;
