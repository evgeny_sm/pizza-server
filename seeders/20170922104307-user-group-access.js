'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('userGroupAccesses', [{
            code: 'admin.view',
            group: 1,
            description: 'Доступ к административной части',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            code: 'catalog.edit',
            group: 1,
            description: 'Редактирование каталога',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            code: 'order.view',
            group: 1,
            description: 'Просмотр заказов',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            code: 'file.upload',
            group: 1,
            description: 'Загрузка файлов',
            createdAt: new Date(),
            updatedAt: new Date(),
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('userGroupAccesses', {id: [1, 2, 3]}, {});
    }
};
