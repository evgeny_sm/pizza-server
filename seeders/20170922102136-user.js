'use strict';
const securityHelper = require('../helpers/securityHelper');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('users', [{
                login: 'userone',
                password: securityHelper.createHash('somepwd'),
                name: 'Test user',
                createdAt: new Date(),
                updatedAt: new Date(),
            },{
                login: 'usertwo',
                password: securityHelper.createHash('somepwd'),
                name: 'Test user two',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('users', null, {});
    }
};
