'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
         return queryInterface.bulkInsert('files', [{
             "name": "3bK_jyw2_x4.jpg",
             "path": "seeders\\file_upload\\3bK_jyw2_x4.jpg",
             "size": 10019,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:20:47.518Z",
             "updatedAt": "2017-09-21T21:20:47.518Z"
         },{
             "name": "CJBz5FuNcUg.jpg",
             "path": "seeders\\file_upload\\CJBz5FuNcUg.jpg",
             "size": 10143,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:29:03.923Z",
             "updatedAt": "2017-09-21T21:29:03.923Z"
         },{
             "name": "ANrD4fDaFtg.jpg",
             "path": "seeders\\file_upload\\ANrD4fDaFtg.jpg",
             "size": 12857,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:24:46.756Z",
             "updatedAt": "2017-09-21T21:24:46.756Z"
         },{
             "name": "ddjW-zza1S0.jpg",
             "path": "seeders\\file_upload\\ddjW-zza1S0.jpg",
             "size": 10832,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:28:15.799Z",
             "updatedAt": "2017-09-21T21:28:15.799Z"
         },{
             "name": "hE3CTpfCGI4.jpg",
             "path": "seeders\\file_upload\\hE3CTpfCGI4.jpg",
             "size": 10799,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:29:29.488Z",
             "updatedAt": "2017-09-21T21:29:29.488Z"
         },{
             "name": "joAC8yM9vhI.jpg",
             "path": "seeders\\file_upload\\joAC8yM9vhI.jpg",
             "size": 10219,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:31:12.943Z",
             "updatedAt": "2017-09-21T21:31:12.943Z"
         }, {
             "name": "OQZ6M4MWr9o.jpg",
             "path": "seeders\\file_upload\\OQZ6M4MWr9o.jpg",
             "size": 10178,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:31:44.134Z",
             "updatedAt": "2017-09-21T21:31:44.134Z"
         },{
             "name": "veHHy4QYrLI.jpg",
             "path": "seeders\\file_upload\\veHHy4QYrLI.jpg",
             "size": 15716,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:32:11.744Z",
             "updatedAt": "2017-09-21T21:32:11.744Z"
         },{
             "name": "-Zb-EAnGSqU.jpg",
             "path": "seeders\\file_upload\\-Zb-EAnGSqU.jpg",
             "size": 13221,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:33:39.365Z",
             "updatedAt": "2017-09-21T21:33:39.365Z"
         },{
             "name": "zlNDquMtHnM.jpg",
             "path": "seeders\\file_upload\\zlNDquMtHnM.jpg",
             "size": 12758,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:34:52.744Z",
             "updatedAt": "2017-09-21T21:34:52.744Z"
         },{
             "name": "VjSOr6_EaQE.jpg",
             "path": "seeders\\file_upload\\VjSOr6_EaQE.jpg",
             "size": 16874,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:36:19.192Z",
             "updatedAt": "2017-09-21T21:36:19.192Z"
         },{
             "name": "Jtotj3g_BdU.jpg",
             "path": "seeders\\file_upload\\Jtotj3g_BdU.jpg",
             "size": 13337,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:36:58.760Z",
             "updatedAt": "2017-09-21T21:36:58.760Z"
         },{
             "name": "dCCssbPwAG4.jpg",
             "path": "seeders\\file_upload\\dCCssbPwAG4.jpg",
             "size": 12980,
             "mimetype": "image/jpeg",
             "createdAt": "2017-09-21T21:38:28.823Z",
             "updatedAt": "2017-09-21T21:38:28.823Z"
         },
         ], {});
    },

    down: (queryInterface, Sequelize) => {
         return queryInterface.bulkDelete('files', null, {});
    }
};
