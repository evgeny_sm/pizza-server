'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('catalogs', [{
            "name": "Норвежская",
            "price": 550,
            "picture": 1,
            "picture_detail": null,
            "description": "Великолепный вкус сёмги и сливочного сыра не оставит вас равнодушными",
            "createdAt": "2017-09-21T21:21:30.986Z",
            "updatedAt": "2017-09-21T21:21:30.986Z"
        }, {
            "name": "Лисичка",
            "price": 470,
            "picture": 2,
            "picture_detail": null,
            "description": "Лисички для лисичек",
            "createdAt": "2017-09-21T21:22:59.209Z",
            "updatedAt": "2017-09-21T21:29:05.072Z"
        }, {
            "name": "Кесадилья с курицей",
            "price": 250,
            "picture": 3,
            "picture_detail": null,
            "description": "Попробуйте мексиканскую экзотику в нашем исполнении",
            "createdAt": "2017-09-21T21:26:30.124Z",
            "updatedAt": "2017-09-21T21:26:30.124Z"
        }, {
            "name": "Рыбная",
            "price": 400,
            "picture": 4,
            "picture_detail": null,
            "description": "Ваши котики будут в восторге",
            "createdAt": "2017-09-21T21:28:56.203Z",
            "updatedAt": "2017-09-21T21:28:56.203Z"
        }, {
            "name": "Фруктовая",
            "price": 350,
            "picture": 5,
            "picture_detail": null,
            "description": "Как пирожки по бабушкиному рецепту",
            "createdAt": "2017-09-21T21:30:09.531Z",
            "updatedAt": "2017-09-21T21:30:09.531Z"
        }, {
            "name": "Щенячье счастье",
            "price": 350,
            "picture": 6,
            "picture_detail": null,
            "description": "Пёсики не останутся равнодушными",
            "createdAt": "2017-09-21T21:31:33.693Z",
            "updatedAt": "2017-09-21T21:31:33.693Z"
        }, {
            "name": "Сырная",
            "price": 450,
            "picture": 7,
            "picture_detail": null,
            "description": "Для настоящих любителей сыра",
            "createdAt": "2017-09-21T21:32:01.767Z",
            "updatedAt": "2017-09-21T21:32:01.767Z"
        }, {
            "name": "Пепперони",
            "price": 450,
            "picture": 8,
            "picture_detail": null,
            "description": "Классика итальянской кулинарии",
            "createdAt": "2017-09-21T21:33:26.830Z",
            "updatedAt": "2017-09-21T21:33:26.830Z"
        }, {
            "name": "Кесадилья грибная",
            "price": 350,
            "picture": 9,
            "picture_detail": null,
            "description": "Кесадилья с белыми грибами, сыром и ветчиной",
            "createdAt": "2017-09-21T21:34:46.258Z",
            "updatedAt": "2017-09-21T21:34:46.258Z"
        }, {
            "name": "Маргарита",
            "price": 450,
            "picture": 10,
            "picture_detail": null,
            "description": "Свежие помидоры и нежная моцарелла... что может быть лучше?",
            "createdAt": "2017-09-21T21:36:02.980Z",
            "updatedAt": "2017-09-21T21:36:02.980Z"
        }, {
            "name": "Картофель-фри",
            "price": 150,
            "picture": 11,
            "picture_detail": null,
            "description": "Картофель фри по нашему секретному рецепту",
            "createdAt": "2017-09-21T21:36:48.732Z",
            "updatedAt": "2017-09-21T21:36:48.732Z"
        }, {
            "name": "Шоколадный кекс",
            "price": 100,
            "picture": 12,
            "picture_detail": null,
            "description": "Такой шоколадный, что слюнки текут",
            "createdAt": "2017-09-21T21:38:23.234Z",
            "updatedAt": "2017-09-21T21:38:23.234Z"
        }, {
            "name": "Кока-кола",
            "price": 120,
            "picture": 13,
            "picture_detail": null,
            "description": "Классика фастфуда",
            "createdAt": "2017-09-21T21:39:01.862Z",
            "updatedAt": "2017-09-21T21:39:01.862Z"
        }], {});

    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('catalogs', null, {});
    }
};
