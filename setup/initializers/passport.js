// Any files in this directory will be `require()`'ed when the application
// starts, and the exported function will be invoked with a `this` context of
// the application itself.  Initializers are used to connect to databases and
// message queues, and configure sub-systems such as authentication.

// Async initializers are declared by exporting `function(done) { /*...*/ }`.
// `done` is a callback which must be invoked when the initializer is
// finished.  Initializers are invoked sequentially, ensuring that the
// previous one has completed before the next one executes.

(function () {
    'use strict';

    /**
     * Module dependencies.
     */

    const passport = require('passport');
    const LocalStrategy = require('passport-local').Strategy;
    const models = require('../../models/index');
    const securityHelper = require('../../helpers/securityHelper');

    // end of dependencies.

    module.exports = function () {

        passport.use(new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password'
        }, function (login, password, done) {
            models.user.findOne({
                where: {login: login},
                include:[{
                    model: models.userGroup,
                    attributes: ['code', 'name'],
                    include:[
                        {
                            model:models.userGroupAccess,
                            attributes: ['code', 'description']
                        }]
                }],
            }).then(user => {
                if(!user) {
                    return done(null, false, {message: 'Incorrect login.'});
                }
                if(securityHelper.checkPassword(password, user.password)) {
                    user.password = null;
                    return done(null, user);
                }
                return done(null, false, {message: 'Incorrect password.'});
            })
        }));

        passport.serializeUser(function (user, done) {
            done(null, user);
        });

        passport.deserializeUser(function (user, done) {
            done(null, user);
        });

    };

})();
