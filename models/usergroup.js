'use strict';
module.exports = (sequelize, DataTypes) => {
    var userGroup = sequelize.define('userGroup', {
        name: DataTypes.STRING,
        code: DataTypes.STRING
    });
    userGroup.associate = function (models) {
        // associations can be defined here
        userGroup.hasMany(models.userGroupAccess, {foreignKey: 'group'});
        userGroup.belongsToMany(models.user, {through: 'userGroupRelations'});
    };
    return userGroup;
};