'use strict';
module.exports = (sequelize, DataTypes) => {
    var orderItem = sequelize.define('orderItem', {
        orderId: DataTypes.INTEGER,
        catalogItemId: DataTypes.INTEGER,
        name: DataTypes.STRING,
        count: DataTypes.INTEGER,
        price: DataTypes.INTEGER,
        sum: DataTypes.INTEGER
    });
    orderItem.associate = function (models) {
        // associations can be defined here
        orderItem.belongsTo(models.order, {foreignKey: 'orderId', as: 'orderObject'});
        orderItem.belongsTo(models.catalog, {foreignKey: 'catalogItemId'});
    };
    return orderItem;
};