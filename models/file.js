'use strict';
module.exports = (sequelize, DataTypes) => {
    var file = sequelize.define('file', {
        name: DataTypes.STRING,
        path: DataTypes.STRING,
        size: DataTypes.INTEGER,
        mimetype: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        }
    });
    return file;
};