'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('user', {
        login: DataTypes.STRING,
        password: DataTypes.STRING,
        name: DataTypes.STRING
    });
    User.associate = function (models) {
        // associations can be defined here
        User.belongsToMany(models.userGroup, {through: 'userGroupRelations'});
    };

    return User;
};