'use strict';
const crypto = require('crypto');
module.exports = (sequelize, DataTypes) => {
    var order = sequelize.define('order', {
        street: DataTypes.STRING,
        house: DataTypes.STRING,
        apart: DataTypes.STRING,
        name: DataTypes.STRING,
        phone: DataTypes.STRING,
        email: DataTypes.STRING,
        note: DataTypes.STRING,
        sum: DataTypes.INTEGER,
        token: {
            type: DataTypes.STRING,
            defaultValue: () =>  crypto.randomBytes(8).toString('hex')
        }
    });
    order.associate = function (models) {
        // associations can be defined here
        order.hasMany(models.orderItem, {foreignKey: 'orderId'});
    };
    return order;
};