'use strict';
module.exports = (sequelize, DataTypes) => {
    var catalog = sequelize.define('catalog', {
        name: DataTypes.STRING,
        price: DataTypes.INTEGER,
        picture: DataTypes.INTEGER,
        picture_detail: DataTypes.INTEGER,
        description: DataTypes.STRING
    });
    catalog.associate = function (models) {
        // associations can be defined here
        catalog.belongsTo(models.file, {onDelete: "CASCADE", foreignKey: 'picture',  as:'oPicture'});
        catalog.belongsTo(models.file, {onDelete: "CASCADE", foreignKey: 'picture_detail', as:'oPictureDetail'});
    };
    //catalog.sync({force:true});
    return catalog;
};