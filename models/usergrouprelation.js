'use strict';
module.exports = (sequelize, DataTypes) => {
  var userGroupRelation = sequelize.define('userGroupRelation', {
    userId: DataTypes.INTEGER,
    userGroupId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return userGroupRelation;
};