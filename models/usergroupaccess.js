'use strict';
module.exports = (sequelize, DataTypes) => {
    var userGroupAccess = sequelize.define('userGroupAccess', {
        code: DataTypes.STRING,
        group: DataTypes.INTEGER,
        description: DataTypes.STRING
    });
    userGroupAccess.associate = function (models) {
        // associations can be defined here
        userGroupAccess.belongsTo(models.userGroup, {foreignKey: 'group'})
    };
    return userGroupAccess;
};